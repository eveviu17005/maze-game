/* ----------------
  MINIGAME: "MAZE"
---------------- */

/**
 * Sample JSON data for maze challenge
 */
const challenge = {
  challenge_id: 1,
  challenge_title: "Maze Game",
  challenge_description: "Maze Game Description",
  image: "./assets/img/default_bg.jpeg",
  difficulty_level: 0, // start 0 to 3
};
const [LEFT, UP, RIGHT, DOWN] = [37, 38, 39, 40];
const [A, W, D, S] = [65, 87, 68, 83];
var isPlaying = false;
var navLeft = document.getElementById("navLeft");
var navUp = document.getElementById("navUp");
var navDown = document.getElementById("navDown");
var navRight = document.getElementById("navRight");

/**
 * Generate a random number with max value
 *
 * @param {int} max - Maximum value
 * @return Generated random number
 */
function rand(max) {
  return Math.floor(Math.random() * max);
}

/**
 * Shuffles an array
 *
 * @param {Object} arr - Array
 * @return Shuffled array
 */
function shuffle(arr) {
  for (let i = arr.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [arr[i], arr[j]] = [arr[j], arr[i]];
  }
  return arr;
}

/**
 * Adjust brightness of a DOM object
 *
 * @param {int} factor - Brightness factor
 * @param {Object} sprite - DOM of the sprite
 * @return Sprite output
 */
function changeBrightness(factor, sprite) {
  var virtCanvas = document.createElement("canvas");
  virtCanvas.width = 500;
  virtCanvas.height = 500;
  var context = virtCanvas.getContext("2d");
  context.drawImage(sprite, 0, 0, 500, 500);

  var imgData = context.getImageData(0, 0, 500, 500);

  for (let i = 0; i < imgData.data.length; i += 4) {
    imgData.data[i] = imgData.data[i] * factor;
    imgData.data[i + 1] = imgData.data[i + 1] * factor;
    imgData.data[i + 2] = imgData.data[i + 2] * factor;
  }
  context.putImageData(imgData, 0, 0);

  var spriteOutput = new Image();
  spriteOutput.src = virtCanvas.toDataURL();
  virtCanvas.remove();
  return spriteOutput;
}

/**
 * Display feedback message after maze is complete
 *
 * @param {int} moves - Number of moves made to complete the maze
 */
function displayVictoryMess(moves) {
  let points = 10 * moves;
  //TODO: call api
  $("#move_steps").text(moves);
  $("#points").text(points);
  toggleVisablity("Message-Container");
}

/**
 * TODO: To replace with jQuery .toggle() later on.
 */
function toggleVisablity(id) {
  $("#" + id).toggle();
}

/**
 * Generate a maze object
 *
 * @param {int} width - Width of maze
 * @param {int} height - Height of maze
 */
function Maze(Width, Height) {
  var mazeMap;
  var width = Width;
  var height = Height;
  var startCoord, endCoord;
  var dirs = ["n", "s", "e", "w"];
  var modDir = {
    n: {
      y: -1,
      x: 0,
      o: "s",
    },
    s: {
      y: 1,
      x: 0,
      o: "n",
    },
    e: {
      y: 0,
      x: 1,
      o: "w",
    },
    w: {
      y: 0,
      x: -1,
      o: "e",
    },
  };

  /**
   * Retrieve maze object
   * @return The maze
   */
  this.map = function () {
    return mazeMap;
  };

  /**
   * Retrieve start coordinate
   * @return Start coordinate
   */
  this.startCoord = function () {
    return startCoord;
  };

  /**
   * Retrieve end coordinate
   * @return End coordinate
   */
  this.endCoord = function () {
    return endCoord;
  };

  /**
   * Generate a random maze map
   */
  function genMap() {
    mazeMap = new Array(height);
    for (y = 0; y < height; y++) {
      mazeMap[y] = new Array(width);
      for (x = 0; x < width; ++x) {
        mazeMap[y][x] = {
          n: false,
          s: false,
          e: false,
          w: false,
          visited: false,
          priorPos: null,
        };
      }
    }
  }

  /**
   * Maze interaction manager
   */
  function defineMaze() {
    var isComp = false;
    var move = false;
    var cellsVisited = 1;
    var numLoops = 0;
    var maxLoops = 0;
    var pos = {
      x: 0,
      y: 0,
    };
    var numCells = width * height;
    while (!isComp) {
      move = false;
      mazeMap[pos.x][pos.y].visited = true;

      if (numLoops >= maxLoops) {
        shuffle(dirs);
        maxLoops = Math.round(rand(height / 8));
        numLoops = 0;
      }
      numLoops++;
      for (index = 0; index < dirs.length; index++) {
        var direction = dirs[index];
        var nx = pos.x + modDir[direction].x;
        var ny = pos.y + modDir[direction].y;

        if (nx >= 0 && nx < width && ny >= 0 && ny < height) {
          //Check if the tile is already visited
          if (!mazeMap[nx][ny].visited) {
            //Carve through walls from this tile to next
            mazeMap[pos.x][pos.y][direction] = true;
            mazeMap[nx][ny][modDir[direction].o] = true;

            //Set Currentcell as next cells Prior visited
            mazeMap[nx][ny].priorPos = pos;
            //Update Cell position to newly visited location
            pos = {
              x: nx,
              y: ny,
            };
            cellsVisited++;
            //Recursively call this method on the next tile
            move = true;
            break;
          }
        }
      }

      if (!move) {
        //  If it failed to find a direction,
        //  move the current position back to the prior cell and Recall the method.
        pos = mazeMap[pos.x][pos.y].priorPos;
      }
      if (numCells == cellsVisited) {
        isComp = true;
      }
    }
  }

  /**
   * Define start and end coordinate points on the maze map
   */
  function defineStartEnd() {
    switch (rand(4)) {
      case 0:
        startCoord = {
          x: 0,
          y: 0,
        };
        endCoord = {
          x: height - 1,
          y: width - 1,
        };
        break;
      case 1:
        startCoord = {
          x: 0,
          y: width - 1,
        };
        endCoord = {
          x: height - 1,
          y: 0,
        };
        break;
      case 2:
        startCoord = {
          x: height - 1,
          y: 0,
        };
        endCoord = {
          x: 0,
          y: width - 1,
        };
        break;
      case 3:
        startCoord = {
          x: height - 1,
          y: width - 1,
        };
        endCoord = {
          x: 0,
          y: 0,
        };
        break;
    }
  }

  genMap();
  defineStartEnd();
  defineMaze();
}

/**
 * Render the maze object
 *
 * @param {Object} Maze - The maze object
 * @param {Object} ctx - Maze context
 * @param {int} cellsize - Cell size
 */
function DrawMaze(Maze, ctx, cellsize, endSprite = null) {
  var map = Maze.map();
  var cellSize = cellsize;
  var drawEndMethod;
  ctx.lineWidth = cellSize / 40;

  /**
   * Re-render maze with cell size
   * @param {int} size - Cell size
   */
  this.redrawMaze = function (size) {
    cellSize = size;
    ctx.lineWidth = cellSize / 50;
    drawMap();
    drawEndMethod();
  };

  /**
   * Render a single cell
   * @param {int} xCord - x-coordinate
   * @param {int} yCord - y-coordinate
   * @param {Object} cell - The cell object
   */
  function drawCell(xCord, yCord, cell) {
    var x = xCord * cellSize;
    var y = yCord * cellSize;

    if (cell.n == false) {
      ctx.beginPath();
      ctx.moveTo(x, y);
      ctx.lineTo(x + cellSize, y);
      ctx.stroke();
    }
    if (cell.s === false) {
      ctx.beginPath();
      ctx.moveTo(x, y + cellSize);
      ctx.lineTo(x + cellSize, y + cellSize);
      ctx.stroke();
    }
    if (cell.e === false) {
      ctx.beginPath();
      ctx.moveTo(x + cellSize, y);
      ctx.lineTo(x + cellSize, y + cellSize);
      ctx.stroke();
    }
    if (cell.w === false) {
      ctx.beginPath();
      ctx.moveTo(x, y);
      ctx.lineTo(x, y + cellSize);
      ctx.stroke();
    }
  }

  /**
   * Render the map
   */
  function drawMap() {
    for (x = 0; x < map.length; x++) {
      for (y = 0; y < map[x].length; y++) {
        drawCell(x, y, map[x][y]);
      }
    }
  }

  /**
   * Render the ending sprite
   */
  function drawEndSprite() {
    var offsetLeft = cellSize / 50;
    var offsetRight = cellSize / 25;
    var coord = Maze.endCoord();
    ctx.drawImage(
      endSprite,
      2,
      2,
      endSprite.width,
      endSprite.height,
      coord.x * cellSize + offsetLeft,
      coord.y * cellSize + offsetLeft,
      cellSize - offsetRight,
      cellSize - offsetRight
    );
  }
  drawEndMethod = drawEndSprite;
  drawMap();
  drawEndMethod();
}

/**
 * Render the player as it moves around the map
 *
 * @param {Object} maze - The maze object
 * @param {Object} mazeCanvas - The maze canvas
 * @param {int} _cellsize - Cell size
 * @param {int} onComplete - Method to execute when rendering is complete
 * @param {Object} sprite - The player sprite image object
 */
function Player(maze, mazeCanvas, _cellsize, onComplete, sprite = null) {
  var ctx = mazeCanvas.getContext("2d");
  var drawSprite;
  var moves = 0;
  drawSprite = drawSpriteImg;
  var player = this;
  var map = maze.map();
  var cellCoords = {
    x: maze.startCoord().x,
    y: maze.startCoord().y,
  };
  var cellSize = _cellsize;

  this.redrawPlayer = function (_cellsize) {
    cellSize = _cellsize;
    drawSpriteImg(cellCoords);
  };

  function drawSpriteImg(coord) {
    console.log(coord);
    var offsetLeft = cellSize / 50;
    var offsetRight = cellSize / 25;
    ctx.drawImage(
      sprite,
      0,
      0,
      sprite.width,
      sprite.height,
      coord.x * cellSize + offsetLeft,
      coord.y * cellSize + offsetLeft,
      cellSize - offsetRight,
      cellSize - offsetRight
    );
    console.log(maze.endCoord());
    console.log(coord.x === maze.endCoord().x);
    console.log(coord.y === maze.endCoord().y);
    if (coord.x === maze.endCoord().x && coord.y === maze.endCoord().y) {
      onComplete(moves);

      player.unbindKeyDown();
    }
  }

  function removeSprite(coord) {
    var offsetLeft = cellSize / 50;
    var offsetRight = cellSize / 25;
    ctx.globalAlpha = 0.2;
    ctx.clearRect(
      coord.x * cellSize + offsetLeft,
      coord.y * cellSize + offsetLeft,
      cellSize - offsetRight,
      cellSize - offsetRight
    );
    ctx.globalAlpha = 1.0;
  }

  function checkPlayerMove(keyboardEvent) {
    var cell = map[cellCoords.x][cellCoords.y];
    //cell return the object with true/false value that show the way player can move
    moves++;
    if (isPlaying) {
      switch (keyboardEvent.keyCode) {
        case A:
        case LEFT:
          if (cell.w == true) {
            removeSprite(cellCoords);
            cellCoords = {
              x: cellCoords.x - 1,
              y: cellCoords.y,
            };
            drawSprite(cellCoords);
          }
          break;
        case W:
        case UP:
          if (cell.n == true) {
            removeSprite(cellCoords);
            cellCoords = {
              x: cellCoords.x,
              y: cellCoords.y - 1,
            };
            drawSprite(cellCoords);
          }
          break;
        case D:
        case RIGHT:
          if (cell.e == true) {
            removeSprite(cellCoords);
            cellCoords = {
              x: cellCoords.x + 1,
              y: cellCoords.y,
            };
            drawSprite(cellCoords);
          }
          break;
        case S:
        case DOWN:
          if (cell.s == true) {
            removeSprite(cellCoords);
            cellCoords = {
              x: cellCoords.x,
              y: cellCoords.y + 1,
            };
            drawSprite(cellCoords);
          }
          break;
      }
    }
  }
  this.bindKeyDown = function () {
    window.addEventListener("keydown", checkPlayerMove, false);

    /* The above code is adding event listeners to the nav buttons. */
    ["click", "touchstart"].forEach((evt) => {
      navLeft.addEventListener(
        evt,
        () => {
          checkPlayerMove({ keyCode: LEFT });
        },
        false
      );
    });
    ["click", "touchstart"].forEach((evt) => {
      navRight.addEventListener(
        evt,
        () => {
          checkPlayerMove({ keyCode: RIGHT });
        },
        false
      );
    });
    ["click", "touchstart"].forEach((evt) => {
      navUp.addEventListener(
        evt,
        () => {
          checkPlayerMove({ keyCode: UP });
        },
        false
      );
    });
    ["click", "touchstart"].forEach((evt) => {
      navDown.addEventListener(
        evt,
        () => {
          checkPlayerMove({ keyCode: DOWN });
        },
        false
      );
    });
    // navLeft.addEventListener("click", () => {
    //   checkPlayerMove({ keyCode: LEFT });
    // });
    // navUp.addEventListener("click", () => {
    //   checkPlayerMove({ keyCode: UP });
    // });
    // navDown.addEventListener("click", () => {
    //   checkPlayerMove({ keyCode: DOWN });
    // });
    // navRight.addEventListener("click", () => {
    //   checkPlayerMove({ keyCode: RIGHT });
    // });

    $("#view").swipe({
      swipe: function (
        event,
        direction,
        distance,
        duration,
        fingerCount,
        fingerData
      ) {
        console.log(direction);
        switch (direction) {
          case "up":
            checkPlayerMove({
              keyCode: UP,
            });
            break;
          case "down":
            checkPlayerMove({
              keyCode: DOWN,
            });
            break;
          case "left":
            checkPlayerMove({
              keyCode: LEFT,
            });
            break;
          case "right":
            checkPlayerMove({
              keyCode: RIGHT,
            });
            break;
        }
      },
      threshold: 0,
    });
  };

  this.unbindKeyDown = function () {
    window.removeEventListener("keydown", checkPlayerMove, false);
    $("#view").swipe("destroy");
  };

  drawSprite(maze.startCoord());
  this.bindKeyDown();
  return moves;
}

/**
 * Render the background image.
 *
 * @param {Object} mazeCanvas - The maze canvas
 * @param {String} imgSrc - Image source
 */
function loadBackground(mazeCanvas, imgSrc) {
  mazeCanvas.style.backgroundImage = `url(${imgSrc})`;
}

var mazeCanvas = $("#mazeCanvas").get(0);
var ctx = mazeCanvas.getContext("2d");
var playerSprite;
var finishSprite;
var maze, draw, player;
var cellSize;
var difficulty;

window.onload = function () {
  let viewWidth = $("#view").width();
  let viewHeight = $("#view").height();
  if (viewHeight < viewWidth) {
    ctx.canvas.width = viewHeight - viewHeight / 100;
    ctx.canvas.height = viewHeight - viewHeight / 100;
  } else {
    ctx.canvas.width = viewWidth - viewWidth / 100;
    ctx.canvas.height = viewWidth - viewWidth / 100;
  }
  //Load and edit sprites
  var completeLoadPlayer = false;
  var completeLoadFinish = false;
  loadBackground(mazeCanvas, challenge.image);
  var isComplete = () => {
    if (completeLoadPlayer === true && completeLoadFinish === true) {
      console.log("Runs");
      setTimeout(function () {
        makeMaze();
      }, 200);
    }
  };
  playerSprite = new Image();
  // character avatar
  playerSprite.src =
    "./assets/img/character_icon.png" + "?" + new Date().getTime();
  playerSprite.setAttribute("crossOrigin", " ");
  playerSprite.onload = function () {
    playerSprite = changeBrightness(1.2, playerSprite);
    completeLoadPlayer = true;
    isComplete();
  };

  finishSprite = new Image();
  // destination image
  finishSprite.src = "./assets/img/end_icon.png" + "?" + new Date().getTime();
  finishSprite.setAttribute("crossOrigin", " ");
  finishSprite.onload = function () {
    finishSprite = changeBrightness(1.1, finishSprite);
    completeLoadFinish = true;
    isComplete();
  };
};

/*
 * Re-renders the maze when the size of the screen is changed
 */
window.onresize = function () {
  let viewWidth = $("#view").width();
  let viewHeight = $("#view").height();
  if (viewHeight < viewWidth) {
    ctx.canvas.width = viewHeight - viewHeight / 100;
    ctx.canvas.height = viewHeight - viewHeight / 100;
  } else {
    ctx.canvas.width = viewWidth - viewWidth / 100;
    ctx.canvas.height = viewWidth - viewWidth / 100;
  }
  cellSize = mazeCanvas.width / difficulty;
  if (player != null) {
    draw.redrawMaze(cellSize);
    player.redrawPlayer(cellSize);
  }
};

/*
 * Generates, renders and displays the maze game on the DOM
 */
function makeMaze() {
  if (player != undefined) {
    player.unbindKeyDown();
    player = null;
  }
  let difficultyArr = [10, 15, 25, 38];
  difficulty = difficultyArr[challenge.difficulty_level];
  console.log(difficulty);
  cellSize = mazeCanvas.width / difficulty;
  maze = new Maze(difficulty, difficulty);
  draw = new DrawMaze(maze, ctx, cellSize, finishSprite);
  player = new Player(
    maze,
    mazeCanvas,
    cellSize,
    displayVictoryMess,
    playerSprite
  );

  if ($("#mazeContainer").css("opacity") < 100) {
    $("#mazeContainer").css("opacity", "100");
  }

  // Show the start button
  $("#startMazeBtn").show();
}

/*
 * Activate the start button
 */
function activateStartMazeGame() {
  isPlaying = true;
  //hide start now button
  $("#startMazeBtn").hide();
}
